#include <Servo.h> 
Servo headServo;
const int HEAD_SERVO_PIN = 8; 
void setup() {
  Serial.begin(9600);
  headServo.attach(HEAD_SERVO_PIN);

}

void loop() {
  for (int deg = 30; deg < 140; deg+=5) {
    headServo.write(deg);
    delay(50);
  }

  // scan left to right
  for (int deg = 140; deg > 30; deg-=5) {
    headServo.write(deg);
    delay(50);
  }

}
